CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS todos (
  id UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  created_time TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
  title VARCHAR(512) NOT NULL,
  done BOOLEAN NOT NULL DEFAULT FALSE
);

-- detects the type of operation and propagate a json body of the relevant row back to the application
CREATE OR REPLACE FUNCTION todo_notify_func()
  RETURNS TRIGGER AS $$
BEGIN
  CASE TG_OP
    WHEN 'INSERT' THEN PERFORM pg_notify('todos_insert', row_to_json(NEW)::text);
    WHEN 'UPDATE' THEN PERFORM pg_notify('todos_update', row_to_json(NEW)::text);
    WHEN 'DELETE' THEN PERFORM pg_notify('todos_delete', row_to_json(OLD)::text);
  END CASE;
  RETURN NULL;
END
$$ LANGUAGE plpgsql;

-- register our notify function to occur after every insert/update/delete
DROP TRIGGER IF EXISTS todos_notify_trig ON todos;
CREATE TRIGGER todos_notify_trig
AFTER INSERT OR UPDATE OR DELETE ON todos
FOR EACH ROW EXECUTE PROCEDURE todo_notify_func();