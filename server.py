"""
A simple example of a flask-based realtime TODO application with a Postgres backend.

We use Postgres with it's native pub/sub capabilities to turn database updates into event propagations back to the ui.
The UI is a simple in-page script built with Vue.js and SocketIO.
  
For simplicity, we use direct database access with the records module.
"""

import os
from threading import Thread

from flask import Flask, jsonify
from flask.globals import request
from flask.templating import render_template
import pgpubsub
import records
import socketio

# setup flask and socket-io; support threading for our background updates
sio = socketio.Server(async_mode='threading')
app = Flask(__name__)
app.wsgi_app = socketio.Middleware(sio, app.wsgi_app)

# connect to the database
db = records.Database("postgres://postgres@localhost:5432/realtimetodo")

# listen for changes in the database
pubsub = pgpubsub.connect(user='postgres', database='realtimetodo')
pubsub.listen('todos_insert')
pubsub.listen('todos_update')
pubsub.listen('todos_delete')


def convert_row_to_todo(row):
    """Converts the given row into a JSON-able dict."""
    return {
        'id':    row.id,
        'title': row.title,
        'done':  row.done
    }


@app.route('/todos', methods=['GET'])
def get_all_todos():
    """Retrieves all of the TODOs from the database."""
    rows = db.query("SELECT id, title, done FROM todos ORDER BY title")
    return jsonify([convert_row_to_todo(row) for row in rows])


@app.route('/todos/<uuid:id>', methods=['GET'])
def get_todo(id):
    """Retrieves a single TODOs from the database."""
    rows = db.query("SELECT id, title, done FROM todos WHERE id = :id LIMIT 1", id=id)
    return jsonify(convert_row_to_todo(rows[0]))


@app.route('/todos', methods=['POST'])
def post_todo():
    """Creates a new TODO in the database."""
    title, done = map(request.json.get, ('title', 'done'))
    db.query("INSERT INTO todos (title, done) VALUES(:title, :done)", title=title, done=done)
    return "Created", 201


@app.route('/todos/<uuid:id>', methods=['PUT'])
def put_todo(id):
    """Updates an existing TODO in the database."""
    title, done = map(request.json.get, ('title', 'done'))
    db.query("UPDATE todos SET title = :title, done=:done WHERE id = :id", id=id, title=title, done=done)
    return "Updated", 202


@app.route('/todos/<uuid:id>', methods=['DELETE'])
def delete_todo(id):
    """Deletes a TODO from the database."""
    db.query("DELETE FROM todos WHERE id = :id", id=id)
    return "Deleted", 204


@app.route('/')
def render_index():
    """Renders the index page."""
    rows = db.query("SELECT id, title, done FROM todos ORDER BY title")
    todos = [convert_row_to_todo(row) for row in rows]
    return render_template('index.html', todos=todos)


def publish_todo_updates():
    """Continually publishes postgres events to websocket clients."""
    while True:
        for event in pubsub.events(yield_timeouts=True):
            if event is not None:
                sio.emit(event.channel, event.payload, skip_sid=True)


if __name__ == '__main__':
    # create database on first run
    db.query_file(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'initdb.sql'))
    # publish updates in the background
    Thread(daemon=True, target=publish_todo_updates).start()
    # start the flask server
    app.run(debug=True, threaded=True)
